galaxy-install:
	ansible-galaxy install -r requirements.yaml --force -g

deploy-server:
	ansible-playbook server.yaml

deploy-server-tags:
	ansible-playbook server.yaml --tags=nginx

deploy-servers:
	ansible-playbook server.yaml -e env=openstack

deploy-servers-tags:
	ansible-playbook server.yaml -e env=openstack --tags=nginx

install-composer:
	ansible-playbook composer.yaml

git-update:
	git pull

update-server: git-update galaxy-install deploy-server